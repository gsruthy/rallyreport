import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/api';
@Component({
  selector: 'app-rally-detail',
  templateUrl: './rally-detail.component.html',
  styleUrls: ['./rally-detail.component.scss']
})
export class RallyDetailComponent implements OnInit {
  version: any[];
  teams: any[];
//  cities2: City[];
  isSelectedVersion: boolean = true;
  selectedVersion: any[] = [];
  isSelectedTeams: boolean = true;
  selectedTeams: any[] = [];
  empList: any[];
  constructor() { }

  ngOnInit() {
    this.version = [
   //   { label: 'TRANSITIONAL MEASURES FOR JHA MATTERS (ARTICLE 10 OF PROTOCOL 36 TO THE TREATIES) ', value: { id: 1, name: 'New York', code: 'NY' } },
      { label: '2019.Q1.4', value: '2019.Q1.4' },
      { label: '2019.Q1.1', value: '2019.Q1.1' },
      { label: '2019.Q1.2', value: '2019.Q1.2' },
      { label: '2019.Q1.3', value:'2019.Q1.3' }
    ];
    this.teams = [
      { label: 'Phoenix', value: 'Phoenix' },
      { label: 'CRO', value:'CRO' },
      { label: 'Hogwarts', value:'Hogwarts' },
      { label: 'Cardinals', value:'Cardinals' }
    ]
    
  }
  loademployeeDetail() {
    this.isSelectedTeams = this.selectedTeams.length>0? true: false;
    this.isSelectedVersion = this.selectedVersion.length>0? true: false;
    if (this.isSelectedTeams && this.isSelectedVersion) {
    this.empList = [
       
      {
          "id": 1,
          "first_name": "40",
          "last_name": "50",
          "email": "125%",
          "phone": "10",
          "address": "10",
          "description": "Customer_1 description"
      },
      {
          "id": 2,
          "first_name": "50",
          "last_name": "40",
          "email": "80%",
          "phone": "5",
          "address": "5",
          "description": "Customer_2 Description"
      }
      // ,
      // {
      //         "id": 3,
      //         "first_name": "Customer_3",
      //         "last_name": "Customer_33",
      //         "email": "customer3@mail.com",
      //         "phone": "00000000000",
      //         "address": "Customer_3 Address",
      //         "description": "Customer_3 description"
      //     },
      //     {
      //         "id": 4,
      //         "first_name": "Customer_4",
      //         "last_name": "Customer_4",
      //         "email": "customer4@mail.com",
      //         "phone": "00000000000",
      //         "address": "Customer_4 Adress",
      //         "description": "Customer_4 Description"
      //     }
    ]
  }
  }
  onVersionChange(event) {
    this.isSelectedVersion = this.selectedVersion.length>0? true: false;
  }
  onTeamChange(event) {
    this.isSelectedTeams = this.selectedTeams.length>0? true: false;
  }
}
