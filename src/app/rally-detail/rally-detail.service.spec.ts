import { TestBed } from '@angular/core/testing';

import { RallyDetailService } from './rally-detail.service';

describe('RallyDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RallyDetailService = TestBed.get(RallyDetailService);
    expect(service).toBeTruthy();
  });
});
