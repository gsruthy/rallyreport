import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RallyDetailComponent } from './rally-detail.component';

describe('RallyDetailComponent', () => {
  let component: RallyDetailComponent;
  let fixture: ComponentFixture<RallyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RallyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RallyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
