import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RallyDetailComponent} from './rally-detail/rally-detail.component'

const routes: Routes = [{ path: '', redirectTo: '/home', pathMatch: 'full' },
{
  path: 'home',
  component: RallyDetailComponent,
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
